# learning-predictive-biomarkers

@ Ellimari Paunio, 2023

This repository contains source code for my Master's thesis https://www.researchgate.net/publication/374913916_Learning_interpretable_predictive_biomarkers_from_multi-omics_data.

The source code for the studied SPKM method can be found from: https://github.com/riikkahuu/spkm.
