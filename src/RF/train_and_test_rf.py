import os
import time
from datetime import date

import numpy as np
import pandas as pd

from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.utils import resample

from sklearn.ensemble import RandomForestClassifier
from sklearn.inspection import permutation_importance
        

def split_train_validation_test(df_m, df_p, y, rand_state):
    train_ratio = 0.7
    validation_ratio = 0.15
    test_ratio = 0.15

    Xtr_m, Xtst_m, ytr, ytst = train_test_split(df_m, y, test_size=(1 - train_ratio), random_state=rand_state, stratify=y)

    Xval_m, Xtst_m, yval, ytst = train_test_split(Xtst_m, ytst, test_size=test_ratio/(test_ratio + validation_ratio), random_state=rand_state, stratify=ytst)

    Xtr_p = np.array(df_p.iloc[Xtr_m.index])
    Xval_p = np.array(df_p.iloc[Xval_m.index])
    Xtst_p = np.array(df_p.iloc[Xtst_m.index])

    Xtr_m = np.array(Xtr_m)
    Xval_m = np.array(Xval_m)
    Xtst_m = np.array(Xtst_m)

    ytr = np.array(ytr)
    yval = np.array(yval)
    ytst = np.array(ytst)

    return Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p, ytr, yval, ytst

def split_train_validation_test_oneview(df, y, rand_state):
    train_ratio = 0.7
    validation_ratio = 0.15
    test_ratio = 0.15

    Xtr, Xtst, ytr, ytst = train_test_split(df, y, test_size=(1 - train_ratio), random_state=rand_state, stratify=y)

    Xval, Xtst, yval, ytst = train_test_split(Xtst, ytst, test_size=test_ratio/(test_ratio + validation_ratio), random_state=rand_state, stratify=ytst)

    Xtr = np.array(Xtr)
    Xval = np.array(Xval)
    Xtst = np.array(Xtst)

    ytr = np.array(ytr)
    yval = np.array(yval)
    ytst = np.array(ytst)

    return Xtr, Xval, Xtst, ytr, yval, ytst

def stab_selec_samples(Xtr, ytr, arr_job_id):
    
    Xtr, ytr = resample(Xtr, ytr,replace=False,n_samples=Xtr.shape[0]/2,random_state=arr_job_id, stratify=ytr)
    
    return Xtr, ytr 

def train_and_validate(Xtr, Xval, ytr, yval, rand_state, max_depth):
    
    results = {'balanced_acc': [], 'sensitivity': [], 'specificity': [],
              'tn': [], 'fp': [], 'fn': [], 'tp': [], 'f1': []}

    rf = RandomForestClassifier(max_depth=max_depth, random_state=rand_state)
    rf.fit(Xtr, ytr)
        
    preds = rf.predict(Xval)
    
    feat_importances = rf.feature_importances_
        
    bal_acc = balanced_accuracy_score(yval, preds)
    conf_matrix = confusion_matrix(yval, preds)
    f1 = f1_score(yval, preds)
    tn, fp, fn, tp = conf_matrix.ravel()
    sensitivity = tp / (tp + fn)
    specificity = tn / (tn + fp)
    
    results['balanced_acc'].append(bal_acc)
    results['sensitivity'].append(sensitivity)
    results['specificity'].append(specificity)
    results['tn'].append(tn)
    results['fp'].append(fp)
    results['fn'].append(fn)
    results['tp'].append(tp)
    results['f1'].append(f1)
    
    return results, feat_importances 
    

def save_inputs(inputs, results_path):
    # check whether the specified path exists or not
    isExist = os.path.exists(results_path)
    if not isExist:
        
        # create a new directory because it does not exist
        os.makedirs(results_path)
    
    with open(results_path + "/inputs.txt", 'w') as f:
        print(inputs, file=f)
        
def save_results(results,feat_importances, results_path):
    df_results = pd.DataFrame(results)
    
    df_results.to_csv(results_path + "/valid_results.csv")
    
    np.savetxt(results_path + "/feature_importances.txt", feat_importances)

        
    print("Results saved!")
    
def save_test_results(test_results, results_path):
    # check whether the specified path exists or not
    isExist = os.path.exists(results_path)
    if not isExist:
        
        # create a new directory because it does not exist
        os.makedirs(results_path)
        
        df_results = pd.DataFrame(test_results)
        df_results.to_csv(results_path + "/test_results.csv")
    
        print("Results saved!")
    else:
        print("No results saved!")


          
        


