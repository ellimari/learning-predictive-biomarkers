import numpy as np
import pandas as pd
import h5py
import os

from preprocess_rf import *
from train_and_test_rf import *


inputs = {"data": 'biobanq_full.hdf5',
        "only_metabolites": False,
        "only_proteomics": False,
        "data_path": '../../data/COVID/2023-02-20/',
        "remove_constants": True,
        "remove_near_constants": True,
        "near_constant_thr": 0.95,
        "remove_low_var_features": True,
        "rsd_threshold": 0.15,
        "log_transformation": True,
        "change_labels": True,
        "standardization": True,
        "min_max_0_1": False,
        "balance_training": True,
        "random_state": 0,
        "max_depth": None,
        "results_path": "../../results/RF/COVID/" + str(date.today())
         }

# load data
df_m, df_p, y, f = load_covid_data(inputs["data"], inputs["data_path"])

# preprocess data
if inputs["remove_constants"]:
    df_m, df_p = remove_constants(df_m, df_p)
    
if inputs["remove_near_constants"]:
    df_m, df_p = remove_near_constants(df_m, df_p, inputs["near_constant_thr"])
    
if inputs["remove_low_var_features"]:
    df_m, df_p = remove_low_var_features(df_m, df_p, inputs["rsd_threshold"])
    
if inputs["log_transformation"]:
    df_m, df_p = log_transformation(df_m, df_p)
    
if inputs["change_labels"]:
    y = change_labels(y)
    
# train, validation, test split
Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p, ytr, yval, ytst = split_train_validation_test(df_m, df_p, y, inputs["random_state"])

if inputs["only_metabolites"]:
    # standardize data
    if inputs['standardization']:
        Xtr, Xval, Xtst =  standardization(Xtr_m, Xval_m, Xtst_m)
    # scale to [0,1]
    if inputs['min_max_0_1']:
        Xtr, Xval, Xtst = min_max_0_1(Xtr_m, Xval_m, Xtst_p)      
elif inputs["only_proteomics"]:
    # standardize data
    if inputs['standardization']:
        Xtr, Xval, Xtst =  standardization(Xtr_p, Xval_p, Xtst_p)
    # scale to [0,1]
    if inputs['min_max_0_1']:
        Xtr, Xval, Xtst = min_max_0_1(Xtr_p, Xval_p, Xtst_p)   
else: 
    Xtr = np.append(Xtr_m, Xtr_p, axis=1)
    Xval = np.append(Xval_m, Xval_p, axis=1)
    Xtst = np.append(Xtst_m, Xtst_p, axis=1)
    
    # standardize data
    if inputs['standardization']:
        Xtr, Xval, Xtst =  standardization(Xtr, Xval, Xtst)
    # scale to [0,1]
    if inputs['min_max_0_1']:
        Xtr, Xval, Xtst = min_max_0_1(Xtr, Xval, Xtst)   

# balance data before training
if inputs["balance_training"]:
    Xtr, ytr = balance_training(Xtr, ytr, inputs["random_state"])
    
results, feat_importances = train_and_validate(Xtr, Xval, ytr, yval, inputs["random_state"], inputs["max_depth"])

save_inputs(inputs, inputs["results_path"])
save_results(results,feat_importances, inputs["results_path"])