import numpy as np
import pandas as pd

from preprocess_logistic_regression import *
from train_and_test_logistic_regression import *

from datetime import date

inputs = {"data": 'biobanq_full.hdf5',
        "only_metabolites": False,
        "only_proteomics": False,
        "data_path": '../../data/COVID/2023-02-20/',
        "remove_constants": True,
        "remove_near_constants": True,
        "near_constant_thr": 0.95,
        "remove_low_var_features": True,
        "rsd_threshold": 0.15,
        "log_transformation": True,
        "change_labels": True,
        "standardization": True,
        "min_max_0_1": False,
        "balance_training": True,
        "random_states": [0,1,2,3,4],
        "Cs": np.logspace(5, -2, 20),
        "results_path": "../../results/Logistic_regression/COVID/GRID-SEARCH/" + str(date.today())
         }

# load data
df_m, df_p, y, f = load_covid_data(inputs["data"], inputs["data_path"])

# preprocess data
if inputs["remove_constants"]:
    df_m, df_p = remove_constants(df_m, df_p)
    
if inputs["remove_near_constants"]:
    df_m, df_p = remove_near_constants(df_m, df_p, inputs["near_constant_thr"])
    
if inputs["remove_low_var_features"]:
    df_m, df_p = remove_low_var_features(df_m, df_p, inputs["rsd_threshold"])
    
if inputs["log_transformation"]:
    df_m, df_p = log_transformation(df_m, df_p)
    
if inputs["change_labels"]:
    y = change_labels(y)
    
# grid search
results, mean_bal_accs = grid_search(df_m, df_p, y, inputs["Cs"], inputs["random_states"], inputs)

save_grid_search_results(results, mean_bal_accs, inputs, inputs['results_path'])