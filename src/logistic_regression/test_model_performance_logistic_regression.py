import numpy as np
import pandas as pd

from preprocess_logistic_regression import *
from train_and_test_logistic_regression import *

from datetime import date

inputs = {"data": 'biobanq_full.hdf5',
        "only_metabolites": False,
        "only_proteomics": False,
        "data_path": '../../data/COVID/2023-02-20/',
        "remove_constants": True,
        "remove_near_constants": True,
        "near_constant_thr": 0.95,
        "remove_low_var_features": True,
        "rsd_threshold": 0.15,
        "log_transformation": True,
        "change_labels": True,
        "standardization": True,
        "min_max_0_1": False,
        "balance_training": True,
        "random_state": 0,
        "Cs": np.logspace(5, -2, 100),
        "results_path": "../../results/Logistic_regression/COVID/" + str(date.today())
         }

# load data
df_m, df_p, y, f = load_covid_data(inputs["data"], inputs["data_path"])

# preprocess data
if inputs["remove_constants"]:
    df_m, df_p = remove_constants(df_m, df_p)

if inputs["remove_near_constants"]:
    df_m, df_p = remove_near_constants(df_m, df_p, inputs["near_constant_thr"])

if inputs["remove_low_var_features"]:
    df_m, df_p = remove_low_var_features(df_m, df_p, inputs["rsd_threshold"])


df_p = df_p.iloc[:, [1,2,3]] # replace 1,2,3 with selected features
df_m = df_m.iloc[:, [1,2,3]] # replace 1,2,3 with selected features
      
if inputs["log_transformation"]:
    df_m, df_p = log_transformation(df_m, df_p)
    
if inputs["change_labels"]:
    y = change_labels(y)
    
df = pd.concat([df_m, df_p], axis=1)

#df = df_p if only proteins

# train, validation, test split
Xtr, Xval, Xtst, ytr, yval, ytst = split_train_validation_test_oneview(df, y, inputs["random_state"])

# standardize data
if inputs['standardization']:
    Xtr, Xval, Xtst =  standardization(Xtr, Xval, Xtst)
# scale to [0,1]
if inputs['min_max_0_1']:
    Xtr, Xval, Xtst = min_max_0_1(Xtr, Xval, Xtst)   
    
if inputs["balance_training"]:
    Xtr_resampled, ytr_resampled = balance_training(Xtr, ytr, inputs["random_state"])

tst_results = train_and_validate(Xtr_resampled, Xtst, ytr_resampled, ytst, inputs["random_state"])

# save test results
save_validation_results(tst_results, inputs["results_path"])