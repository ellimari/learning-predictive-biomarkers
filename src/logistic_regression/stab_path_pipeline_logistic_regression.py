import numpy as np
import pandas as pd

from preprocess_logistic_regression import *
from train_and_test_logistic_regression import *

from datetime import date

inputs = {"data": 'biobanq_full.hdf5',
        "only_metabolites": False,
        "only_proteomics": False,
        "data_path": '../../data/COVID/2023-02-20/',
        "remove_constants": True,
        "remove_near_constants": True,
        "near_constant_thr": 0.95,
        "remove_low_var_features": True,
        "rsd_threshold": 0.15,
        "log_transformation": True,
        "change_labels": True,
        "standardization": True,
        "min_max_0_1": False,
        "balance_training": True,
        "random_state": 0,
        "Cs": np.logspace(5, -2, 100),
        "results_path": "../../results/Logistic_regression/COVID/" + str(date.today())
         }

# load data
df_m, df_p, y, f = load_covid_data(inputs["data"], inputs["data_path"])

# preprocess data
if inputs["remove_constants"]:
    df_m, df_p = remove_constants(df_m, df_p)
    
if inputs["remove_near_constants"]:
    df_m, df_p = remove_near_constants(df_m, df_p, inputs["near_constant_thr"])
    
if inputs["remove_low_var_features"]:
    df_m, df_p = remove_low_var_features(df_m, df_p, inputs["rsd_threshold"])
    
if inputs["log_transformation"]:
    df_m, df_p = log_transformation(df_m, df_p)
    
if inputs["change_labels"]:
    y = change_labels(y)

# train, validation, test split
Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p, ytr, yval, ytst = split_train_validation_test(df_m, df_p, y, inputs["random_state"])

if inputs["only_metabolites"]:
    # standardize data
    if inputs['standardization']:
        Xtr, Xval, Xtst =  standardization(Xtr_m, Xval_m, Xtst_m)
    # scale to [0,1]
    if inputs['min_max_0_1']:
        Xtr, Xval, Xtst = min_max_0_1(Xtr_m, Xval_m, Xtst_p)      
elif inputs["only_proteomics"]:
    # standardize data
    if inputs['standardization']:
        Xtr, Xval, Xtst =  standardization(Xtr_p, Xval_p, Xtst_p)
    # scale to [0,1]
    if inputs['min_max_0_1']:
        Xtr, Xval, Xtst = min_max_0_1(Xtr_p, Xval_p, Xtst_p)   
else: 
    Xtr = np.append(Xtr_m, Xtr_p, axis=1)
    Xval = np.append(Xval_m, Xval_p, axis=1)
    Xtst = np.append(Xtst_m, Xtst_p, axis=1)
    
    # standardize data
    if inputs['standardization']:
        Xtr, Xval, Xtst =  standardization(Xtr, Xval, Xtst)
    # scale to [0,1]
    if inputs['min_max_0_1']:
        Xtr, Xval, Xtst = min_max_0_1(Xtr, Xval, Xtst)   

# stability selection subsamples
all_coefs = []
all_valid_bal_accs = []
for exp_num in range(1, 101): # 100 subsamples
    print("exp_num:", exp_num)
    Xtr_sample, ytr_sample = stab_selec_samples(Xtr, ytr, exp_num)
    # balance data before training
    if inputs["balance_training"]:
        Xtr_resampled, ytr_resampled = balance_training(Xtr_sample, ytr_sample, inputs["random_state"])
    # regularization path for one subsample
    coefs, valid_bal_accs = regularization_path(Xtr_resampled, Xval, ytr_resampled, yval, inputs["Cs"], inputs["random_state"]) 
    all_coefs.append(coefs)
    all_valid_bal_accs.append(valid_bal_accs)
    #save_reg_path_results(experiment_num, inputs["fold_i"], coefs, inputs["results_path"])
    
save_inputs(inputs, inputs["results_path"])

bal_accs = np.array(all_valid_bal_accs)
avg_bal_accs = np.sum(bal_accs, axis=0)/100
save_valid_balanced_accuracies(avg_bal_accs, inputs["results_path"])

reg_paths = np.expand_dims(all_coefs[0], axis=0)
for i in range(1,100):
    reg_path = np.expand_dims(all_coefs[i], axis=0)
    reg_paths = np.concatenate((reg_paths, reg_path))  

stab_path = stability_path(reg_paths)

# save stab paths
save_stab_path(stab_path, inputs["results_path"])