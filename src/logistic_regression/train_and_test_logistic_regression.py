import os
import time
from datetime import date

import numpy as np
import pandas as pd

from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.utils import resample
from imblearn.under_sampling import RandomUnderSampler
from preprocess_logistic_regression import *

from sklearn import linear_model

    
def load_grid_search_results(results_path):
    mean_bal_accs = np.loadtxt(results_path + '/mean_bal_accs.txt')
    df_results = pd.read_csv(results_path + '/results.csv')
    
    return mean_bal_accs, df_results

def split_train_validation_test(df_m, df_p, y, rand_state):
    train_ratio = 0.7
    validation_ratio = 0.15
    test_ratio = 0.15

    Xtr_m, Xtst_m, ytr, ytst = train_test_split(df_m, y, test_size=(1 - train_ratio), random_state=rand_state, stratify=y)

    Xval_m, Xtst_m, yval, ytst = train_test_split(Xtst_m, ytst, test_size=test_ratio/(test_ratio + validation_ratio), random_state=rand_state, stratify=ytst)

    Xtr_p = np.array(df_p.iloc[Xtr_m.index])
    Xval_p = np.array(df_p.iloc[Xval_m.index])
    Xtst_p = np.array(df_p.iloc[Xtst_m.index])

    Xtr_m = np.array(Xtr_m)
    Xval_m = np.array(Xval_m)
    Xtst_m = np.array(Xtst_m)

    ytr = np.array(ytr)
    yval = np.array(yval)
    ytst = np.array(ytst)

    return Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p, ytr, yval, ytst

def split_train_validation_test_oneview(df, y, rand_state):
    train_ratio = 0.7
    validation_ratio = 0.15
    test_ratio = 0.15

    Xtr, Xtst, ytr, ytst = train_test_split(df, y, test_size=(1 - train_ratio), random_state=rand_state, stratify=y)

    Xval, Xtst, yval, ytst = train_test_split(Xtst, ytst, test_size=test_ratio/(test_ratio + validation_ratio), random_state=rand_state, stratify=ytst)

    Xtr = np.array(Xtr)
    Xval = np.array(Xval)
    Xtst = np.array(Xtst)

    ytr = np.array(ytr)
    yval = np.array(yval)
    ytst = np.array(ytst)

    return Xtr, Xval, Xtst, ytr, yval, ytst

def stab_selec_samples(Xtr, ytr, arr_job_id):
    
    Xtr, ytr = resample(Xtr, ytr,replace=False,n_samples=Xtr.shape[0]/2,random_state=arr_job_id, stratify=ytr)
    
    return Xtr, ytr 

def balance_training(Xtr_sample, ytr_sample, rand_state):
    rus = RandomUnderSampler(random_state=rand_state)
    Xtr_resampled, ytr_resampled = rus.fit_resample(Xtr_sample, ytr_sample)
    
    return Xtr_resampled, ytr_resampled

def regularization_path(Xtr, Xval, ytr, yval, cs, rand_state):
    clf = linear_model.LogisticRegression(
    penalty="l1",
    solver="liblinear",
    #tol=1e-6,
    random_state=rand_state,
    max_iter=int(1e6),
    #warm_start=True,
)
    coefs = []
    bal_accs = []
    for c in cs:
        clf.set_params(C=c)
        clf.fit(Xtr, ytr)
        y_pred = clf.predict(Xval)
        bal_acc = balanced_accuracy_score(yval, y_pred) 
        bal_accs.append(bal_acc)
        #print("Balanced accuracy:", bal_acc)
        coefs.append(clf.coef_.ravel().copy())
    
    return coefs, bal_accs


def stability_path(reg_paths):
    non_zero_count = np.count_nonzero(reg_paths, axis=0)
    
    stab_path = non_zero_count/100

    return stab_path

def grid_search(df_m, df_p, y, regparams, rand_states, inputs):
    
    results = {'random_state': [],'reg_param': [], 'balanced_acc': [], 'feat_idxs': []}
    
    all_bal_accs = []
    
    for rand_state in rand_states:
        
        bal_accs = []
        # train, validation, test split
        Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p, ytr, yval, ytst = split_train_validation_test(df_m, df_p, y, rand_state)

        if inputs["only_metabolites"]:
            # standardize data
            if inputs['standardization']:
                Xtr, Xval, Xtst =  standardization(Xtr_m, Xval_m, Xtst_m)
            # scale to [0,1]
            if inputs['min_max_0_1']:
                Xtr, Xval, Xtst = min_max_0_1(Xtr_m, Xval_m, Xtst_p)      
        elif inputs["only_proteomics"]:
            # standardize data
            if inputs['standardization']:
                Xtr, Xval, Xtst =  standardization(Xtr_p, Xval_p, Xtst_p)
            # scale to [0,1]
            if inputs['min_max_0_1']:
                Xtr, Xval, Xtst = min_max_0_1(Xtr_p, Xval_p, Xtst_p)   
        else: 
            Xtr = np.append(Xtr_m, Xtr_p, axis=1)
            Xval = np.append(Xval_m, Xval_p, axis=1)
            Xtst = np.append(Xtst_m, Xtst_p, axis=1)

            # standardize data
            if inputs['standardization']:
                Xtr, Xval, Xtst =  standardization(Xtr, Xval, Xtst)
            # scale to [0,1]
            if inputs['min_max_0_1']:
                Xtr, Xval, Xtst = min_max_0_1(Xtr, Xval, Xtst)
        
        if inputs["balance_training"]:
            Xtr, ytr = balance_training(Xtr, ytr, rand_state)
        
        clf = linear_model.LogisticRegression(
            penalty="l1",
            solver="liblinear",
            random_state=rand_state,
            max_iter=int(1e6),
        )

        for regparam in regparams:

            print("regparam:", regparam)
            
            clf.set_params(C=regparam)
            clf.fit(Xtr, ytr)
            y_pred = clf.predict(Xval)
            
            bal_acc = balanced_accuracy_score(yval, y_pred) 
            bal_accs.append(bal_acc)
            print("balanced acc:", bal_acc)

            coefs = clf.coef_.ravel().copy()
            feat_idxs = np.where(coefs != 0)

            print("feat idxs:", feat_idxs)
            
            results['random_state'].append(rand_state)  
            results['reg_param'].append(regparam)    
            results['balanced_acc'].append(bal_acc)
            results['feat_idxs'].append(feat_idxs)
        
        all_bal_accs.append(bal_accs)
        mean_bal_accs = np.mean(np.array(all_bal_accs), axis=0)

    return results, mean_bal_accs

def get_selected_features(reg_idxs, df_results):

    feat_idxs = df_results.loc[reg_idxs, 'feat_idxs'].values
    
    return feat_idxs

def train_and_test_fold(Xtr, Xtst, ytr, ytst, rand_state):
    
    results = {'balanced_acc': [], 'sensitivity': [], 'specificity': [],
              'tn': [], 'fp': [], 'fn': [], 'tp': []}

    clf = linear_model.LogisticRegression(penalty='none',random_state=rand_state, max_iter=int(1e6)).fit(Xtr, ytr)
        
    y_pred = clf.predict(Xtst)
        
    bal_acc = balanced_accuracy_score(ytst, y_pred) 
    conf_matrix = confusion_matrix(ytst, y_pred)
    tn, fp, fn, tp = conf_matrix.ravel()
    sensitivity = tp / (tp + fn)
    specificity = tn / (tn + fp)
    
    results['balanced_acc'].append(bal_acc)
    results['sensitivity'].append(sensitivity)
    results['specificity'].append(specificity)
    results['tn'].append(tn)
    results['fp'].append(fp)
    results['fn'].append(fn)
    results['tp'].append(tp)
    
    return results

def train_and_validate(Xtr, Xval, ytr, yval, rand_state):
    
    results = {'balanced_acc': [], 'sensitivity': [], 'specificity': [],
              'tn': [], 'fp': [], 'fn': [], 'tp': [], 'f1': []}

    clf = linear_model.LogisticRegression(penalty='none',random_state=rand_state, max_iter=int(1e6)).fit(Xtr, ytr)
        
    y_pred = clf.predict(Xval)
        
    bal_acc = balanced_accuracy_score(yval, y_pred) 
    conf_matrix = confusion_matrix(yval, y_pred)
    f1 = f1_score(yval, y_pred)
    tn, fp, fn, tp = conf_matrix.ravel()
    sensitivity = tp / (tp + fn)
    specificity = tn / (tn + fp)
    
    results['balanced_acc'].append(bal_acc)
    results['sensitivity'].append(sensitivity)
    results['specificity'].append(specificity)
    results['tn'].append(tn)
    results['fp'].append(fp)
    results['fn'].append(fn)
    results['tp'].append(tp)
    results['f1'].append(f1)
    
    return results
          
def save_inputs(inputs, results_path):
    # check whether the specified path exists or not
    isExist = os.path.exists(results_path)
    if not isExist:
        
        # create a new directory because it does not exist
        os.makedirs(results_path)
    
    with open(results_path + "/inputs.txt", 'w') as f:
        print(inputs, file=f)
        
def save_stab_path(stab_path, results_path):
    np.savetxt(results_path + "/stability_path.txt", stab_path)
        
    print("Results saved!") 
    
def save_valid_balanced_accuracies(avg_bal_accs, results_path):
    
    np.savetxt(results_path + "/avg_bal_accs.txt", avg_bal_accs)
    
    print("Avg balanced accuracies saved!")
    
def save_final_results(results, results_path):
    df_results = pd.DataFrame(results)
    
    df_results.to_csv(results_path + "/final_results.csv")
        
    print("Results saved!")
    
def save_validation_results(valid_results, results_path):
    # check whether the specified path exists or not
    isExist = os.path.exists(results_path)
    if not isExist:
        
        # create a new directory because it does not exist
        os.makedirs(results_path)
        
        df_results = pd.DataFrame(valid_results)
        df_results.to_csv(results_path + "/validation_results.csv")
    
        print("Results saved!")
    else:
        print("No results saved!")

    
def save_grid_search_results(results, mean_bal_accs, inputs, results_path):
    df_results = pd.DataFrame(results)
    
    # check whether the specified path exists or not
    isExist = os.path.exists(results_path)
    if not isExist:
        
        # create a new directory because it does not exist
        os.makedirs(results_path)
        
        with open(results_path + "/inputs.txt", 'w') as f:
            print(inputs, file=f)
        
        np.savetxt(results_path + "/mean_bal_accs.txt", mean_bal_accs)
        
        df_results.to_csv(results_path + "/results.csv")
        
        print("Results saved!")
    else:
        print("No results saved!")

          
        


