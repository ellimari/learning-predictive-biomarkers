#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --mem=500M
#SBATCH --job-name=stab-path-logistic-regression
#SBATCH --output=stab_path_%j.out

module load anaconda

srun python stab_path_pipeline_logistic_regression.py 
