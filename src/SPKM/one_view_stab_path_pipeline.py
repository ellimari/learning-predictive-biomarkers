from train_and_test import *

results_path = "../../results/SPKM/POLY/COVID/ONE-VIEW/" + "2023-05-29" # change date

reg_path_u = load_reg_path(1, results_path)
df_results = load_reg_path_results(1, results_path)
bal_accs = np.array(df_results.loc[:, 'balanced_acc'])

reg_paths_u = np.expand_dims(reg_path_u, axis=0)

for exp_num in range(2,101): # 100 subsamples
    reg_path_u = load_reg_path(exp_num, results_path)
    reg_path_u = np.expand_dims(reg_path_u, axis=0)
    reg_paths_u = np.concatenate((reg_paths_u, reg_path_u))
 
    
    df_results = load_reg_path_results(exp_num, results_path)
    bal_accs += np.array(df_results.loc[:, 'balanced_acc'])
    
avg_bal_accs = bal_accs/100

stab_path_u= count_stability_path_one_view(reg_paths_u)

save_stab_path_one_view(stab_path_u, results_path)

save_balanced_accuracies(avg_bal_accs, results_path)
