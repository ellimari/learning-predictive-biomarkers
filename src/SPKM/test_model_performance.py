import sys
import numpy as np
import pandas as pd
from preprocess import *
from train_and_test import *
from kernels_and_gradients import PolyKernel
from datetime import date

inputs = {"data": 'biobanq_full.hdf5',
        "data_path": '../../data/COVID/2023-02-20/',
        "remove_constants": True, 
        "remove_near_constants": False,
        "near_constant_thr": 0.95,
        "remove_low_var_features": False,
        "rsd_threshold": 0.15,
        "log_transformation": True,
        "change_labels": True,
        "standardization": True,
        "min_max_0_1": False,
        "n_u": 2,
        "k1": {"d":3, "r":1},
        "k2": {"d":3, "r":1},
        "spkminit": "rand",
        "nspkminits": 10,
        "prepr": [],
        "tr_balanced": False,
        "balance_training": True,
        "max_outer_iters": 5,
        "random_state": 2,
        "regstyle": "l1ball",
        "regparam_ranges": [np.logspace(0, -4, 100), np.logspace(0, -4, 100)],
        "results_path": "../../results/SPKM/POLY/COVID/" + str(date.today()) # change date
         }


# load data
df_m, df_p, y, f = load_covid_data(inputs["data"], inputs["data_path"])

# preprocess data
if inputs["remove_constants"]:
    df_m, df_p = remove_constants(df_m, df_p)

if inputs["remove_near_constants"]:
    df_m, df_p = remove_near_constants(df_m, df_p, inputs["near_constant_thr"])

if inputs["remove_low_var_features"]:
    df_m, df_p = remove_low_var_features(df_m, df_p, inputs["rsd_threshold"])

df_m = df_m.iloc[:, [1,2,3]] # replace 1,2,3 with selected features
df_p = df_p.iloc[:, [1,2,3]] # replace 1,2,3 with selected features

if inputs["log_transformation"]:
    df_m, df_p = log_transformation(df_m, df_p)

if inputs["change_labels"]:
    y = change_labels(y)
    
# train, validation, test split
Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p, ytr, yval, ytst = split_train_validation_test(df_m, df_p, y, inputs["random_state"])

# standardize data
if inputs["standardization"]:
    Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p = standardization(Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p) 
    print("data standardized")
    
if inputs["min_max_0_1"]:
    Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p = standardization(Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p) 
    print("data to [0,1]")
    
# balance data before training
if inputs["balance_training"]:
    Xtr_m, Xtr_p, ytr = balance_training(Xtr_m, Xtr_p, ytr, inputs["random_state"])
    
# train and validate
regparams = [1,1]
tst_results = train_and_validate(Xtr_m, Xtst_m, Xtr_p, Xtst_p, ytr, ytst, inputs["n_u"], PolyKernel(inputs["k1"]), PolyKernel(inputs["k2"]), inputs["spkminit"], inputs["nspkminits"], inputs["prepr"], inputs["tr_balanced"], inputs["max_outer_iters"], inputs["regstyle"], regparams)

# save test results
save_validation_results(tst_results, inputs["results_path"])


