import numpy as np
import pandas as pd
import h5py

from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler


def load_covid_data(file_name, data_path):
    f = h5py.File(data_path + file_name, 'r')
    X_m = np.array(f["View0"])
    X_p = np.array(f["View1"])
    
    ids_m = np.array(f["Metadata"]["feature_ids-View0"])
    ids_p = np.array(f["Metadata"]["feature_ids-View1"])
    
    column_m_names = []
    column_p_names = []

    for column_m_name in ids_m:
        tmp = str(column_m_name)[2:]
        column_m_names.append(tmp[:-1])
        
    for column_p_name in ids_p:
        tmp =  str(column_p_name)[2:]
        column_p_names.append(tmp[:-1])
        
    y = np.array(f["Labels"])

    df_m = pd.DataFrame(X_m, columns=column_m_names)
    df_p = pd.DataFrame(X_p, columns=column_p_names)
    
    return df_m, df_p, y, f

def remove_constants(df_m, df_p):
    df_m = df_m.loc[:, (df_m != df_m.iloc[0]).any()]
    df_p = df_p.loc[:, (df_p != df_p.iloc[0]).any()]
    
    return df_m, df_p
    
def remove_near_constants(df_m, df_p, thr):
    sample_size = df_p.shape[0]
    
    df_m = df_m.loc[:, df_m.apply(lambda x: (x.value_counts() < thr*sample_size).all())]
    df_p = df_p.loc[:, df_p.apply(lambda x: (x.value_counts() < thr*sample_size).all())]
    
    return df_m, df_p

def remove_low_var_features(df_m, df_p, rsd_threshold):
    rsd_m = df_m.std()/df_m.mean()
    rsd_p = df_p.std()/df_p.mean()

    df_m = df_m.loc[:, rsd_m >= rsd_threshold] 
    df_p = df_p.loc[:, rsd_p >= rsd_threshold] 
    
    return df_m, df_p

def log_transformation(df_m, df_p):
    df_m = np.log10(df_m)
    df_p = np.log10(df_p)
    
    return df_m, df_p

def standardization(Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p):
    sc = StandardScaler() 
    
    Xtr_m = sc.fit_transform(Xtr_m) 
    Xval_m = sc.transform(Xval_m)
    Xtst_m = sc.transform(Xtst_m)
    
    Xtr_p = sc.fit_transform(Xtr_p)
    Xval_p = sc.transform(Xval_p)
    Xtst_p = sc.transform(Xtst_p)
    
    return Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p

def standardization_one_view(Xtr, Xval, Xtst):
    sc = StandardScaler() 
    Xtr = sc.fit_transform(Xtr) 
    Xval = sc.transform(Xval)
    Xtst = sc.transform(Xtst)
    
    return Xtr, Xval, Xtst

def min_max_0_1_one_view(Xtr, Xval, Xtst):
    mm = MinMaxScaler()
    Xtr = mm.fit_transform(Xtr) 
    Xval = mm.transform(Xval)
    Xtst = mm.transform(Xtst)
    
    return Xtr, Xval, Xtst

def min_max_0_1(Xtr_m, Xtst_m, Xtr_p, Xtst_p):
    mm = MinMaxScaler()
    Xtr_m = mm.fit_transform(Xtr_m) 
    Xval_m = mm.transform(Xval_m)
    Xtst_m = mm.transform(Xtst_m)
    
    Xtr_p = mm.fit_transform(Xtr_p)
    Xval_p = mm.transform(Xval_p)
    Xtst_p = mm.transform(Xtst_p)
    
    return Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p

def change_labels(y):
    y = y - 1 # change label 1 to be 0 so that 0 corresponds to individual does not have disease
    y[y==-1] = 1 # change label 0 to be 1 so that 1 corresponds to individual that has disease
    y[y==0] = -1 # 0 must be -1 for SPKM
    
    return y
   
