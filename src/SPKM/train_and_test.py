import os
import time
from datetime import date

import numpy as np
import pandas as pd
import h5py

from sklearn.utils import resample
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split

from kernels_and_gradients import PolyKernel
from spkm_wrapper import pairwiseSPKMlikeSklearn, SPKMlikeSklearn

from imblearn.under_sampling import RandomUnderSampler
from preprocess import *


def load_reg_paths(exp_number, results_path): 
    path = results_path + "/experiment_" + str(exp_number)
    reg_path_u = np.loadtxt(path + '/reg_path_u_' + str(exp_number) + '.txt')
    reg_path_v = np.loadtxt(path + '/reg_path_v_' + str(exp_number) + '.txt')
    
    return reg_path_u, reg_path_v

def load_reg_path(exp_number, results_path): 
    path = results_path + "/experiment_" + str(exp_number)
    reg_path_u = np.loadtxt(path + '/reg_path_u_' + str(exp_number) + '.txt')
    
    return reg_path_u

def load_reg_path_results(exp_number, results_path):
    path = results_path + "/experiment_" + str(exp_number)
    
    df_results = pd.read_csv(path + '/results_' + str(exp_number) + '.csv')
    
    return df_results

def load_grid_search_results(results_path):
    mean_bal_accs = np.loadtxt(results_path + '/mean_bal_accs.txt')
    df_results = pd.read_csv(results_path + '/results.csv')
    
    return mean_bal_accs, df_results

def get_selected_features(reg_idxs, df_results):

    feat_idxs = df_results.loc[reg_idxs, 'feat_idxs'].values
    
    return feat_idxs

def get_selected_features_u_v(reg_idxs, df_results):
    
    feat_idxs_u = df_results.loc[reg_idxs, 'feat_idxs_u'].values
    feat_idxs_v = df_results.loc[reg_idxs, 'feat_idxs_v'].values
    
    return feat_idxs_u, feat_idxs_v

def split_train_validation_test(df_m, df_p, y, rand_state):
    train_ratio = 0.7
    validation_ratio = 0.15
    test_ratio = 0.15

    Xtr_m, Xtst_m, ytr, ytst = train_test_split(df_m, y, test_size=(1 - train_ratio), random_state=rand_state, stratify=y)

    Xval_m, Xtst_m, yval, ytst = train_test_split(Xtst_m, ytst, test_size=test_ratio/(test_ratio + validation_ratio), random_state=rand_state, stratify=ytst)


    Xtr_p = np.array(df_p.iloc[Xtr_m.index])
    Xval_p = np.array(df_p.iloc[Xval_m.index])
    Xtst_p = np.array(df_p.iloc[Xtst_m.index])

    Xtr_m = np.array(Xtr_m)
    Xval_m = np.array(Xval_m)
    Xtst_m = np.array(Xtst_m)

    ytr = np.array(ytr)
    yval = np.array(yval)
    ytst = np.array(ytst)

    return Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p, ytr, yval, ytst

def split_train_validation_test_one_view(df, y, rand_state):
    train_ratio = 0.7
    validation_ratio = 0.15
    test_ratio = 0.15

    Xtr, Xtst, ytr, ytst = train_test_split(df, y, test_size=(1 - train_ratio), random_state=rand_state, stratify=y)

    Xval, Xtst, yval, ytst = train_test_split(Xtst, ytst, test_size=test_ratio/(test_ratio + validation_ratio), random_state=rand_state, stratify=ytst)

    Xtr = np.array(Xtr)
    Xval = np.array(Xval)
    Xtst = np.array(Xtst)

    ytr = np.array(ytr)
    yval = np.array(yval)
    ytst = np.array(ytst)

    return Xtr, Xval, Xtst, ytr, yval, ytst
  

def stab_selec_samples(Xtr_m, Xtr_p, ytr, arr_job_id):
    
    Xtr_m, ytr_m = resample(Xtr_m, ytr,replace=False,n_samples=Xtr_m.shape[0]/2,random_state=arr_job_id, stratify=ytr)
    Xtr_p, ytr_p = resample(Xtr_p, ytr,replace=False,n_samples=Xtr_p.shape[0]/2,random_state=arr_job_id, stratify=ytr)
    
    ytr = ytr_m
    
    return Xtr_m, Xtr_p, ytr 

def balance_training(Xtr_m, Xtr_p, ytr, rand_state):
    rus = RandomUnderSampler(random_state=rand_state)
    Xtr_m, ytr_tmp = rus.fit_resample(Xtr_m, ytr)
    Xtr_p, ytr = rus.fit_resample(Xtr_p, ytr)
    
    return Xtr_m, Xtr_p, ytr

def stab_selec_samples_one_view(Xtr, ytr, arr_job_id):
    
    Xtr, ytr = resample(Xtr, ytr,replace=False,n_samples=Xtr.shape[0]/2,random_state=arr_job_id, stratify=ytr)
    
    return Xtr, ytr 


def balance_training_one_view(Xtr_sample, ytr_sample, rand_state):
    rus = RandomUnderSampler(random_state=rand_state)
    Xtr_resampled, ytr_resampled = rus.fit_resample(Xtr_sample, ytr_sample)
    
    return Xtr_resampled, ytr_resampled


def warm_start_reg_path(Xtr_m, Xval_m, Xtr_p, Xval_p, ytr, yval, n_u, k1, k2, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle, regparam_ranges):
    start = time.time()

    u_list = []
    v_list = []
    results = {'reg_u': [], 'reg_v': [], 'balanced_acc': []} 

    Xtr = {0:Xtr_m, 1:Xtr_p}
    Xval = {0:Xval_m, 1:Xval_p}

    for i in range(len(regparam_ranges[0])):
        print(i)
        if i > 0: # warm start
            spkminit = {"u":u, "v":v}
            
        regparam = [regparam_ranges[0][i], regparam_ranges[1][i]] 
        print("Regularization parameters: ", regparam)

        spkm = pairwiseSPKMlikeSklearn(n_u, k1, k2, regparam, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle)
        spkm.fit(Xtr, ytr)
        
        preds = spkm.predict(Xval)
        preds = np.sign(preds) # predicted labels
        
        bal_acc = balanced_accuracy_score(yval, preds) 
        print("Balanced accuracy:", bal_acc)
        
        u, v = spkm.return_u_v()
        u_list.append((np.abs(u[0]) + np.abs(u[1]))) # when n_u=2
        v_list.append((np.abs(v[0]) + np.abs(v[1]))) # when n_u=2
        
        results['reg_u'].append(regparam[0]) 
        results['reg_v'].append(regparam[0])
        results['balanced_acc'].append(bal_acc)
    reg_path_u = np.array(u_list)
    reg_path_v = np.array(v_list)
    
    stop = time.time()
    print("Time: ", stop - start)

    return reg_path_u, reg_path_v, results

def warm_start_reg_path_one_view(Xtr, Xval, ytr, yval, n_u, k, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle, regparam_ranges):
    start = time.time()

    u_list = []

    results = {'reg_u': [], 'balanced_acc': []} 

    for i in range(len(regparam_ranges)):
        print(i)
        if i > 0: # warm start
            spkminit = {"u": u}
            
        regparam = regparam_ranges[i]
        print("Regularization parameters: ", regparam)

        spkm = SPKMlikeSklearn(n_u, k, regparam, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle)
        spkm.fit(Xtr, ytr)
        
        preds = spkm.predict(Xval)
        preds = np.sign(preds) # predicted labels
        
        bal_acc = balanced_accuracy_score(yval, preds) 
        print("Balanced accuracy:", bal_acc)
        
        u = spkm.return_u()
        u_list.append((np.abs(u[0]) + np.abs(u[1]))) # when n_u=2
        
        results['reg_u'].append(regparam) 
        results['balanced_acc'].append(bal_acc)
    reg_path_u = np.array(u_list)
    
    stop = time.time()
    print("Time: ", stop - start)

    return reg_path_u, results

def count_stability_path(reg_paths_u, reg_paths_v):
    non_zero_u_count = np.count_nonzero(reg_paths_u, axis=0)
    non_zero_v_count = np.count_nonzero(reg_paths_v, axis=0)
    
    stab_path_u = non_zero_u_count/100
    stab_path_v = non_zero_v_count/100
    
    return stab_path_u, stab_path_v

def count_stability_path_one_view(reg_paths_u):
    non_zero_u_count = np.count_nonzero(reg_paths_u, axis=0)
    
    stab_path_u = non_zero_u_count/100
    
    return stab_path_u

def grid_search(df_m, df_p, y, n_u, k1, k2, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle, regparams, rand_states, inputs):
    
    results = {'random_state': [],'reg_params': [], 'balanced_acc': [], 'feat_idxs_u': [], 'feat_idxs_v': []}
    
    all_bal_accs = []
    
    for rand_state in rand_states:
        
        print(rand_state)
        
        bal_accs = []
        # train, validation, test split
        Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p, ytr, yval, ytst = split_train_validation_test(df_m, df_p, y, rand_state)

        # standardize data
        if inputs["standardization"]:
            Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p = standardization(Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p) 
            print("data standardized")
    
        # scale to [0,1]
        if inputs["min_max_0_1"]:
            Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p = standardization(Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p) 
            print("data to [0,1]")
        # balance data before training
        if inputs["balance_training"]:
            Xtr_m, Xtr_p, ytr = balance_training(Xtr_m, Xtr_p, ytr, rand_state)
        
        Xtr = {0:Xtr_m, 1:Xtr_p}
        Xval = {0:Xval_m, 1:Xval_p}
        
        print(regparams)

        for regparam in regparams:

            regparameters = [regparam, regparam] 
            
            print("regparams:", regparameters)
            spkm = pairwiseSPKMlikeSklearn(n_u, k1, k2, regparameters, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle)
            spkm.fit(Xtr, ytr)
        
            preds = spkm.predict(Xval)
            preds = np.sign(preds) # predicted labels
            spkm.fit(Xtr, ytr)
            
            preds[preds==-1] = 0
            yval[yval==-1] = 0

            bal_acc = balanced_accuracy_score(yval, preds) 
            
            bal_accs.append(bal_acc)

            print("balanced acc:", bal_acc)
            
            feats_u, feats_v = spkm.feature_interpretability()

            feat_idxs_u = np.where(feats_u == 1)
            feat_idxs_v = np.where(feats_v == 1)

            print("feat idxs_u:", feat_idxs_u)
            print("feat idxs_v:", feat_idxs_v)
            
            results['random_state'].append(rand_state)  
            results['reg_params'].append(regparam)    
            results['balanced_acc'].append(bal_acc)
            results['feat_idxs_u'].append(feat_idxs_u)
            results['feat_idxs_v'].append(feat_idxs_v)
        
        all_bal_accs.append(bal_accs)
    mean_bal_accs = np.mean(np.array(all_bal_accs), axis=0)

    return results, mean_bal_accs

def grid_search_one_view(df_m, df_p, y, n_u, k, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle, regparams, rand_states, inputs):
    
    results = {'random_state': [],'reg_param': [], 'balanced_acc': [], 'feat_idxs': []}
    
    all_bal_accs = []
    
    for rand_state in rand_states:
        
        bal_accs = []
        # train, validation, test split
        Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p, ytr, yval, ytst = split_train_validation_test(df_m, df_p, y, rand_state)

        Xtr = np.append(Xtr_m, Xtr_p, axis=1)
        Xval = np.append(Xval_m, Xval_p, axis=1)
        Xtst = np.append(Xtst_m, Xtst_p, axis=1)  

        # standardize data
        if inputs['standardization']:
            Xtr, Xval, Xtst = standardization_one_view(Xtr, Xval, Xtst)
        # scale to [0,1]
        if inputs['min_max_0_1']:
            Xtr, Xval, Xtst = min_max_0_1_one_view(Xtr, Xval, Xtst)    
        # balance data before training
        if inputs["balance_training"]:
            Xtr, ytr = balance_training_one_view(Xtr, ytr,rand_state)

        for regparam in regparams:

            print("regparam:", regparam)

            spkm = SPKMlikeSklearn(n_u, k, regparam, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle)
            spkm.fit(Xtr, ytr)

            preds = spkm.predict(Xval)
            preds = np.sign(preds) # predicted labels

            preds[preds==-1] = 0
            yval[yval==-1] = 0

            bal_acc = balanced_accuracy_score(yval, preds) 
            
            bal_accs.append(bal_acc)

            print("balanced acc:", bal_acc)

            feat_idxs = np.where(spkm.feature_interpretability() == 1)

            print("feat idxs:", feat_idxs)
            
            results['random_state'].append(rand_state)  
            results['reg_param'].append(regparam)    
            results['balanced_acc'].append(bal_acc)
            results['feat_idxs'].append(feat_idxs)
        
        all_bal_accs.append(bal_accs)
        mean_bal_accs = np.mean(np.array(all_bal_accs), axis=0)

    return results, mean_bal_accs

def train_and_test_fold(Xtr_m, Xtst_m, Xtr_p, Xtst_p, ytr, ytst, n_u, k1, k2, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle, regparams):
    
    results = {'balanced_acc': [], 'sensitivity': [], 'specificity': [],
              'tn': [], 'fp': [], 'fn': [], 'tp': []}

    Xtr = {0:Xtr_m, 1:Xtr_p}
    Xtst = {0:Xtst_m, 1:Xtst_p}
    
    spkm = pairwiseSPKMlikeSklearn(n_u, k1, k2, regparams, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle)
    spkm.fit(Xtr, ytr)
        
    preds = spkm.predict(Xtst)
    preds = np.sign(preds) # predicted labels
        
    bal_acc = balanced_accuracy_score(ytst, preds) 
    conf_matrix = confusion_matrix(ytst, preds)
    tn, fp, fn, tp = conf_matrix.ravel()
    sensitivity = tp / (tp + fn)
    specificity = tn / (tn + fp)
    
    results['balanced_acc'].append(bal_acc)
    results['sensitivity'].append(sensitivity)
    results['specificity'].append(specificity)
    results['tn'].append(tn)
    results['fp'].append(fp)
    results['fn'].append(fn)
    results['tp'].append(tp)
    
    return results

def train_and_validate(Xtr_m, Xval_m, Xtr_p, Xval_p, ytr, yval, n_u, k1, k2, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle, regparams):
    
    results = {'balanced_acc': [], 'sensitivity': [], 'specificity': [],
              'tn': [], 'fp': [], 'fn': [], 'tp': [], 'f1': []}

    Xtr = {0:Xtr_m, 1:Xtr_p}
    Xval = {0:Xval_m, 1:Xval_p}
    
    spkm = pairwiseSPKMlikeSklearn(n_u, k1, k2, regparams, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle)
    spkm.fit(Xtr, ytr)
        
    preds = spkm.predict(Xval)
    preds = np.sign(preds) # predicted labels
    
    preds[preds==-1] = 0
    yval[yval==-1] = 0
        
    bal_acc = balanced_accuracy_score(yval, preds) 
    f1 = f1_score(yval, preds)
    conf_matrix = confusion_matrix(yval, preds)
    #print(conf_matrix)
    tn, fp, fn, tp = conf_matrix.ravel()
    sensitivity = tp / (tp + fn)
    specificity = tn / (tn + fp)
    
    results['balanced_acc'].append(bal_acc)
    results['sensitivity'].append(sensitivity)
    results['specificity'].append(specificity)
    results['tn'].append(tn)
    results['fp'].append(fp)
    results['fn'].append(fn)
    results['tp'].append(tp)
    results['f1'].append(f1)
    return results

def train_and_validate_one_view(Xtr, Xval, ytr, yval, n_u, k, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle, regparam):
    
    results = {'balanced_acc': [], 'sensitivity': [], 'specificity': [],
              'tn': [], 'fp': [], 'fn': [], 'tp': [], 'f1': []}

    spkm = SPKMlikeSklearn(n_u, k, regparam, spkminit, nspkminits, prepr, tr_balanced, max_outer_iters, regstyle)
    spkm.fit(Xtr, ytr)
        
    preds = spkm.predict(Xval)
    preds = np.sign(preds) # predicted labels
    
    preds[preds==-1] = 0
    yval[yval==-1] = 0
        
    bal_acc = balanced_accuracy_score(yval, preds) 
    conf_matrix = confusion_matrix(yval, preds)
    f1 = f1_score(yval, preds)
    tn, fp, fn, tp = conf_matrix.ravel()
    sensitivity = tp / (tp + fn)
    specificity = tn / (tn + fp)
    
    results['balanced_acc'].append(bal_acc)
    results['sensitivity'].append(sensitivity)
    results['specificity'].append(specificity)
    results['tn'].append(tn)
    results['fp'].append(fp)
    results['fn'].append(fn)
    results['tp'].append(tp)
    results['f1'].append(f1)
    
    return results
        
def save_reg_path_results(arr_job_id, inputs, reg_path_u, reg_path_v, results, results_path):
    df_results = pd.DataFrame(results)

    path = results_path + "/experiment_" + str(arr_job_id)
    
    # check whether the specified path exists or not
    isExist = os.path.exists(path)
    if not isExist:
        
        # create a new directory because it does not exist
        os.makedirs(path)
        
        with open(path + "/inputs_" + str(arr_job_id) + ".txt", 'w') as f:
            print(inputs, file=f)
        
        np.savetxt(path + "/reg_path_u_" + str(arr_job_id) + ".txt", reg_path_u)
        np.savetxt(path + "/reg_path_v_" + str(arr_job_id) + ".txt", reg_path_v)
        
        df_results.to_csv(path + "/results_" + str(arr_job_id) + ".csv")
        
        print("Results saved!")
    else:
        print("No results saved!")
        
def save_reg_path_results_one_view(arr_job_id, inputs, reg_path_u, results, results_path):
    df_results = pd.DataFrame(results)

    path = results_path + "/experiment_" + str(arr_job_id)
    
    # check whether the specified path exists or not
    isExist = os.path.exists(path)
    if not isExist:
        
        # create a new directory because it does not exist
        os.makedirs(path)
        
        with open(path + "/inputs_" + str(arr_job_id) + ".txt", 'w') as f:
            print(inputs, file=f)
        
        np.savetxt(path + "/reg_path_u_" + str(arr_job_id) + ".txt", reg_path_u)
        
        df_results.to_csv(path + "/results_" + str(arr_job_id) + ".csv")
        
        print("Results saved!")
    else:
        print("No results saved!")
    
def save_stab_path(stab_path_u, stab_path_v, results_path):
   
    np.savetxt(results_path + "/stab_path_u.txt", stab_path_u)
    np.savetxt(results_path + "/stab_path_v.txt", stab_path_v)
        
    print("Results saved!")
    
def save_stab_path_one_view(stab_path_u, results_path):
   
    np.savetxt(results_path + "/stab_path_u.txt", stab_path_u)
        
    print("Results saved!")
    
def save_balanced_accuracies(avg_bal_accs, results_path):
    
    np.savetxt(results_path + "/avg_bal_accs.txt", avg_bal_accs)
    
    print("Avg balanced accuracies saved!")
    
def save_validation_results(valid_results, results_path):
    # check whether the specified path exists or not
    isExist = os.path.exists(results_path)
    if not isExist:
        
        # create a new directory because it does not exist
        os.makedirs(results_path)
        
        df_results = pd.DataFrame(valid_results)
        df_results.to_csv(results_path + "/validation_results.csv")
    
        print("Results saved!")
    else:
        print("No results saved!")
    
def save_grid_search_results(results, mean_bal_accs, inputs, results_path):
    df_results = pd.DataFrame(results)
    
    # check whether the specified path exists or not
    isExist = os.path.exists(results_path)
    if not isExist:
        
        # create a new directory because it does not exist
        os.makedirs(results_path)
        
        with open(results_path + "/inputs.txt", 'w') as f:
            print(inputs, file=f)
        
        np.savetxt(results_path + "/mean_bal_accs.txt", mean_bal_accs)
        
        df_results.to_csv(results_path + "/results.csv")
        
        print("Results saved!")
    else:
        print("No results saved!")
    
    
    

    



          
        


