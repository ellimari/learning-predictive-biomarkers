from train_and_test import *

results_path = "../../results/SPKM/POLY/COVID/" + "2023-05-30" # change date 

reg_path_u, reg_path_v = load_reg_paths(1, results_path)
df_results = load_reg_path_results(1, results_path)
bal_accs = np.array(df_results.loc[:, 'balanced_acc'])

reg_paths_u = np.expand_dims(reg_path_u, axis=0)
reg_paths_v = np.expand_dims(reg_path_v, axis=0)

for exp_num in range(2,101): # 100 subsamples
    reg_path_u, reg_path_v = load_reg_paths(exp_num, results_path)
    reg_path_u = np.expand_dims(reg_path_u, axis=0)
    reg_path_v = np.expand_dims(reg_path_v, axis=0)
    reg_paths_u = np.concatenate((reg_paths_u, reg_path_u))
    reg_paths_v = np.concatenate((reg_paths_v, reg_path_v))
    
    df_results = load_reg_path_results(exp_num, results_path)
    bal_accs += np.array(df_results.loc[:, 'balanced_acc'])
    
avg_bal_accs = bal_accs/100

stab_path_u, stab_path_v = count_stability_path(reg_paths_u, reg_paths_v)

save_stab_path(stab_path_u, stab_path_v, results_path)

save_balanced_accuracies(avg_bal_accs, results_path)
