import sys
import numpy as np
import pandas as pd
from preprocess import *
from train_and_test import *
from kernels_and_gradients import PolyKernel
from datetime import date

inputs = {"data": 'biobanq_full.hdf5',
        "data_path": '../../data/COVID/2023-02-20/',
        "remove_constants": True, 
        "remove_near_constants": True,
        "near_constant_thr": 0.95,
        "remove_low_var_features": True,
        "rsd_threshold": 0.15,
        "log_transformation": True,
        "change_labels": True,
        "standardization": True,
        "min_max_0_1": False,
        "n_u": 2,
        "k": {"d":3, "r":1},
        "spkminit": "rand",
        "nspkminits": 10,
        "prepr": [],
        "tr_balanced": False,
        "balance_training": True,
        "max_outer_iters": 5,
        "random_state": 3,
        "regstyle": "l1ball",
        "regparam_ranges": np.logspace(0, -5, 100),
        "results_path": "../../results/SPKM/POLY/COVID/ONE-VIEW/" + str(date.today())
         }

# load data
df_m, df_p, y, f = load_covid_data(inputs["data"], inputs["data_path"])

if inputs["remove_constants"]:
    df_m, df_p = remove_constants(df_m, df_p)

if inputs["remove_near_constants"]:
    df_m, df_p = remove_near_constants(df_m, df_p, inputs["near_constant_thr"])

if inputs["remove_low_var_features"]:
    df_m, df_p = remove_low_var_features(df_m, df_p, inputs["rsd_threshold"])

    
df_m = df_m.iloc[:, [1,2,3]] # replace 1,2,3 with selected features            
df_p = df_p.iloc[:, [1,2,3]] # replace 1,2,3 with selected features
                

if inputs["log_transformation"]:
    df_m, df_p = log_transformation(df_m, df_p)

if inputs["change_labels"]:
    y = change_labels(y)
                 
df = pd.concat([df_m, df_p], axis=1)

# df = df_p, if only proteins

# train, validation, test split
Xtr, Xval, Xtst, ytr, yval, ytst = split_train_validation_test_one_view(df, y, inputs["random_state"])
    
# standardize data
if inputs['standardization']:
    Xtr, Xval, Xtst =  standardization_one_view(Xtr, Xval, Xtst)
# scale to [0,1]
if inputs['min_max_0_1']:
    Xtr, Xval, Xtst = min_max_0_1_one_view(Xtr, Xval, Xtst)   
    
if inputs["balance_training"]:
    Xtr, ytr = balance_training_one_view(Xtr, ytr, inputs["random_state"])

# train and test
regparam = 1
tst_results = train_and_validate_one_view(Xtr, Xtst, ytr, ytst, inputs["n_u"], PolyKernel(inputs["k"]), inputs["spkminit"], inputs["nspkminits"], inputs["prepr"], inputs["tr_balanced"], inputs["max_outer_iters"], inputs["regstyle"], regparam)

# save test results
save_validation_results(tst_results, inputs["results_path"])