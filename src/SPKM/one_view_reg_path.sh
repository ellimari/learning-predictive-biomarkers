#!/bin/bash
#SBATCH --time=07:00:00
#SBATCH --mem=800M
#SBATCH --job-name=one-view-reg-path-2023-05-29
#SBATCH --output=one-view-2023-05-29/reg-path_%a.out
#SBATCH --array=1-100

module load anaconda

srun python one_view_reg_path_pipeline.py ${SLURM_ARRAY_TASK_ID} 
