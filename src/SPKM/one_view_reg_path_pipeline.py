import sys
import numpy as np
import pandas as pd
from preprocess import *
from train_and_test import *
from kernels_and_gradients import PolyKernel
from datetime import date

inputs = {"data": 'biobanq_full.hdf5',
        "data_path": '../../data/COVID/2023-02-20/',
        "remove_constants": True, 
        "remove_near_constants": True,
        "near_constant_thr": 0.95,
        "remove_low_var_features": True,
        "rsd_threshold": 0.15,
        "log_transformation": True,
        "change_labels": True,
        "standardization": True,
        "min_max_0_1": False,
        "n_u": 2,
        "k": {"d":3, "r":1},
        "spkminit": "rand",
        "nspkminits": 10,
        "prepr": [],
        "tr_balanced": False,
        "balance_training": True,
        "max_outer_iters": 5,
        "random_state": 3,
        "regstyle": "l1ball",
        "regparam_ranges": np.logspace(0, -5, 100),
        "results_path": "../../results/SPKM/POLY/COVID/ONE-VIEW/" + str(date.today())
         }

# load data
df_m, df_p, y, f = load_covid_data(inputs["data"], inputs["data_path"])

# preprocess data
if inputs["remove_constants"]:
    df_m, df_p = remove_constants(df_m, df_p)
    
if inputs["remove_near_constants"]:
    df_m, df_p = remove_near_constants(df_m, df_p, inputs["near_constant_thr"])
    
if inputs["remove_low_var_features"]:
    df_m, df_p = remove_low_var_features(df_m, df_p, inputs["rsd_threshold"])
    
if inputs["log_transformation"]:
    df_m, df_p = log_transformation(df_m, df_p)
    
if inputs["change_labels"]:
    y = change_labels(y)
    
# train, validation, test split
Xtr_m, Xval_m, Xtst_m, Xtr_p, Xval_p, Xtst_p, ytr, yval, ytst = split_train_validation_test(df_m, df_p, y, inputs["random_state"])

Xtr = np.append(Xtr_m, Xtr_p, axis=1)
Xval = np.append(Xval_m, Xval_p, axis=1)
Xtst = np.append(Xtst_m, Xtst_p, axis=1)
    
# standardize data
if inputs['standardization']:
    Xtr, Xval, Xtst =  standardization_one_view(Xtr, Xval, Xtst)
    
# scale to [0,1]
if inputs['min_max_0_1']:
    Xtr, Xval, Xtst = min_max_0_1_one_view(Xtr, Xval, Xtst)  
    
# stability selection subsamples
arr_job_id = int(sys.argv[1])
Xtr, ytr = stab_selec_samples_one_view(Xtr, ytr, arr_job_id)

# balance data before training
if inputs["balance_training"]:
    Xtr, ytr = balance_training_one_view(Xtr, ytr, inputs["random_state"])

# regularization path for one subsample
reg_path_u, results = warm_start_reg_path_one_view(Xtr, Xval, ytr, yval, inputs["n_u"], PolyKernel(inputs["k"]), inputs["spkminit"], inputs["nspkminits"], inputs["prepr"], inputs["tr_balanced"], inputs["max_outer_iters"], inputs["regstyle"], inputs["regparam_ranges"])

# save results 
save_reg_path_results_one_view(arr_job_id, inputs, reg_path_u, results, inputs["results_path"])
