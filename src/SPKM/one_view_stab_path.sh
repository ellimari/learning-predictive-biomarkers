#!/bin/bash
#SBATCH --time=00:05:00
#SBATCH --mem=800M
#SBATCH --job-name=one-view-stab-path-2023-05-29
#SBATCH --output=one-view-2023-05-29/stab_path.out

module load anaconda

srun python one_view_stab_path_pipeline.py
