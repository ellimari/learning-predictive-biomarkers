#!/bin/bash
#SBATCH --time=05:00:00
#SBATCH --mem=800M
#SBATCH --job-name=reg-path-2023-05-28
#SBATCH --output=2023-05-28/reg-path_%a.out
#SBATCH --array=1-100

module load anaconda

srun python reg_path_pipeline.py ${SLURM_ARRAY_TASK_ID} 
