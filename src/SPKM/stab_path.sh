#!/bin/bash
#SBATCH --time=00:05:00
#SBATCH --mem=800M
#SBATCH --job-name=stab_path_2023-05-30
#SBATCH --output=2023-05-30/stab_path.out

module load anaconda

srun python stab_path_pipeline.py
